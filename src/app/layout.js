import './globals.css'

// font
import { Pacifico } from 'next/font/google'
import { Dancing_Script } from 'next/font/google'
const pacifico = Pacifico({ subsets: ['latin'], weight: ['400'] })
const ds = Dancing_Script({ subsets: ['latin'], weight: ['400', '700'] })
// end font

export const metadata = {
  title: 'Web Blog With NextJS',
  description: 'Website Blogger With Framework NextJS',
}

export default function RootLayout({ children }) {
  return (
    <html lang="en">
      <head>
      <link rel="icon" href="/vercel.svg" type="image/<generated>" sizes="<generated>"/>
      </head>
      <body>
        <div className='text-center mt-5'>
        <div className={pacifico.className}> LAYOUT PACIFICO</div>
        <div className={ds.className}>LAYOUT Dancing Script</div>
        </div>
        {children}
      </body>
    </html>
  )
}
