import React from 'react'
import Slider from './components/Homepage/slider'
import Column from './components/Homepage/column'

const Main = () => {
  return (
    <div>
      <div>
        <Column />
        <Slider />
        </div>
    </div>
  )
}

export default Main