import React from 'react'
import Image from 'next/image'

// font
import { Inter } from 'next/font/google'
const inter = Inter({ subsets: ['latin'], weight: ['400','800'] })
// end font

const Column = () => {
  return (
    <div className={inter.className}>
        <h2 className='font-[700] w-full max-w-[970px] mx-auto'>
            Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.
        </h2>
        <div className='w-full max-w-[970px] mx-auto flex flex-wrap my-5'>
          <div className='w-6/12 md:w-4/12'>
              <Image src="https://picsum.photos/250/300" alt="Image 1" width={250} height={300}
              className='w-full block max-w-[250px] mx-auto min-h-[300px] object-cover my-5' />        
          </div>
          <div className='w-6/12 md:w-4/12'>
              <Image src="https://picsum.photos/250/300" alt="Image 1" width={250} height={300}
              className='w-full block max-w-[250px] mx-auto min-h-[300px] object-cover my-5' />        
          </div>
          <div className='w-6/12 md:w-4/12'>
              <Image src="https://picsum.photos/250/300" alt="Image 1" width={250} height={300}
              className='w-full block max-w-[250px] mx-auto min-h-[300px] object-cover my-5' />        
          </div>
          <div className='w-6/12 md:w-4/12'>
              <Image src="https://picsum.photos/250/300" alt="Image 1" width={250} height={300}
              className='w-full block max-w-[250px] mx-auto min-h-[300px] object-cover my-5' />        
          </div>
          <div className='w-6/12 md:w-4/12'>
              <Image src="https://picsum.photos/250/300" alt="Image 1" width={250} height={300}
              className='w-full block max-w-[250px] mx-auto min-h-[300px] object-cover my-5' />        
          </div>
          <div className='w-6/12 md:w-4/12'>
              <Image src="https://picsum.photos/250/300" alt="Image 1" width={250} height={300}
              className='w-full block max-w-[250px] mx-auto min-h-[300px] object-cover my-5' />        
          </div>
        </div>
    </div>
  )
}

export default Column