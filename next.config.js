/** @type {import('next').NextConfig} */
const nextConfig = {
    images: {
        remotePatterns: [
            {
                hostname: "picsum.photos"
            },
            {
                hostname: "placehold.co"
            }
        ]
    }
}

module.exports = nextConfig
